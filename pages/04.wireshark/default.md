---
title: Wireshark
media_order: '1024px-Wireshark_icon.svg.png,chrome_2019-11-03_16-30-39.png'
---

![](1024px-Wireshark_icon.svg.png)


### Table des matières


### Wireshark est
* Un logitiel d'analyse de paquets envoyer sur un réseaux internet.
* Cappble de reconnaitre **1515 protocoles**.
* Supporter par **tous** les systèmes d'éxploitations baseés sur **UNIX et Winodws**.

### L'histoire de Wireshark

Gerald Combs trouvait exorbitant les prix pour les logitiels d'analyse de protocoles, il a donc fait le sien par lui meme.

Sortie initiale en 1998 sous le nom de **Ethereal**.

2006, Combs accepte de travailler avec **CASE Technologies**,il amene son code source de Ethereal mais le renomme Wireshark à cause de problèmes de droits d'auteur.

Combs a eu de l'aide de plus de **600 personnes** pour la version actuelle de Wireshark.

### Wireshark est utile parce que

* Il ressemble beaucoup à plusieurs autres logitiel similaires
* Il est open source et gratuit
* Il a un interface graphique qui aide la comprehension de l'information
* Il existe un version sans interface appellé Tshark

### Installation et utilisation de base
* Un guide d'installation complet en anglais est disponible <a href="https://en.wikiversity.org/wiki/Wireshark/Install"><b>ici</b></a>

* Un guide d'instalation complet en francais est disponible <a href="https://www.youtube.com/watch?v=EPUu7IyS4wc"><b>ici</b></a>

* Un guide d'utilisation de base est disponible [**ici**](../plus-dinformation)